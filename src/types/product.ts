export type MediaType = "video" | "image";

export interface ProductMedia {
  id: string;
  type: MediaType;
  url: string;
}

export interface Product {
  id: string;
  media: ProductMedia[];
  name: string;
  order: number;
  vendor: string;
}
