export interface Promotion {
  text: string;
  order: number;
  hide: boolean;
}
