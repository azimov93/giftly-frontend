import { Route, Switch } from "react-router-dom";
import styles from "./App.module.scss";
import MainPage from "../../pages/MainPage";
import ListPage from "../../pages/ListPage";
import ProductPage from "../../pages/ProductPage";

const MAIN_ROUTE = "/";
const LIST_ROUTE = "/products";
const PRODUCT_ROUTE = "/products/:id";

const App = () => {
  return (
    <main className={styles.layout}>
      <Switch>
        <Route exact path={MAIN_ROUTE}>
          <MainPage />
        </Route>
        <Route exact path={LIST_ROUTE}>
          <ListPage />
        </Route>
        <Route exact path={PRODUCT_ROUTE}>
          <ProductPage />
        </Route>
      </Switch>
    </main>
  )
};

export default App;
