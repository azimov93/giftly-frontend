import {useMemo, useState} from "react";
import { Link } from "react-router-dom";
import ReactPlayer from "react-player";
import { Product } from "../../types/product";
import styles from "./ProductCard.module.scss";

const ProductCard = (props: Product) => {
  const {
    id,
    media,
    name,
    order,
    vendor,
  } = props;

  const [showControls, setControls] = useState(false);

  const preferredMedia = useMemo(
    () => media.find(item => item.type === "video") || media[0],
    [media]
  );

  return (
    <div
      style={{
        order
      }}
      className={styles.productWrapper}
    >
      <div
        className={styles.productMedia}
        onMouseEnter={() => setControls(true)}
        onMouseLeave={() => setControls(false)}
      >
        {preferredMedia.type === "video" ?
          (
            <ReactPlayer
              url={preferredMedia.url}
              id={preferredMedia.id}
              stopOnUnmount
              width="100%"
              height="100%"
              controls={showControls}
              config={{
                file: {
                  attributes: {
                    preload: "none",
                    poster: media.find(item => item.type === "image")?.url
                  }
                }
              }}
            />
          ) : (
            <img
              className={styles.productImage}
              src={preferredMedia.url}
              id={preferredMedia.id}
              alt={name}
            />
          )
        }
      </div>
      <div className={styles.information}>
        <span className={styles.productVendor}>{vendor}</span>
        <span className={styles.productName}>{name}</span>
        <Link
          to={`/products/${id}`}
          className={styles.link}
        >
          See More
        </Link>
      </div>
    </div>
  )
};

export default ProductCard;
