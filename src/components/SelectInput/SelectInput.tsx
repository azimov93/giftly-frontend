import styles from "./SelectInput.module.scss";
import { SelectHTMLAttributes } from "react";

interface Props extends SelectHTMLAttributes<HTMLSelectElement> {
  options: string[]
}

const SelectInput = ({ options, ...props }: Props) => {
  return (
    <div className={styles.selectBox}>
      <select className={styles.selectInput} {...props}>
        <option id="default" value="">All vendors</option>
        {options.map(item => (
          <option key={item} id={item} value={item}>{item}</option>
        ))}
      </select>
      <div className={styles.selectIcon}><i className="fa fa-sort-down"/></div>
    </div>
  )
};

export default SelectInput;
