import { Promotion } from "../../types/promotion";
import image from "../../assets/img.png";
import styles from "./Promotion.module.scss";

const PromotionItem = (props: Promotion) => {
  const {
    text,
    order
  } = props;

  return (
    <div
      style={{
        order
      }}
      className={styles.promotion}
    >
      <div className={styles.image}>
        <img src={image} alt={text} />
      </div>
      <span className={styles.text}>{text}</span>
    </div>
  )
};

export default PromotionItem;
