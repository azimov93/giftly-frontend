import { InputHTMLAttributes } from "react";
import styles from "./SearchInput.module.scss";

const SearchInput = (props: InputHTMLAttributes<HTMLInputElement>) => {
  return (
    <div className={styles.searchBox}>
      <div className={styles.searchIcon}><i className="fa fa-search"/></div>
      <input className={styles.searchInput} type="text" {...props} />
    </div>
  )
}

export default SearchInput;
