import { useQuery } from "react-query";
import { Link, useParams } from "react-router-dom";
import Loader from "../../components/Loader";
import * as api from './modules/api';
import styles from "./ProductPage.module.scss"
import ReactPlayer from "react-player";

const ProductPage = () => {
  const { id } = useParams<{ id: string }>();
  const { isLoading, isSuccess, data = null } = useQuery(
    `product-${id}`,
    () => api.getProduct(id),
    {
      retry: true,
      refetchOnWindowFocus: false,
    }
  );

  return (
    <div className={styles.page}>
      <Link
        to="/products"
        className={styles.backLink}
      >
        Go to List
      </Link>
      {isLoading && <Loader />}
      {isSuccess && (
        <div className={styles.content}>
          <div className={styles.titles}>
            <span className={styles.name}>{data?.name}</span>
            <span className={styles.vandor}>{data?.vendor}</span>
          </div>
          <div className={styles.media}>
            {data?.media?.map(item => {
              if (item.type === "video") {
                return (
                  <div className={styles.mediaItem}>
                    <ReactPlayer
                      url={item.url}
                      id={item.id}
                      controls
                      width="100%"
                      config={{
                        file: {
                          attributes: {
                            preload: "metadata",
                          }
                        }
                      }}
                    />
                  </div>
                )
              }

              return (
                <div className={styles.media}>
                  <img
                    className={styles.image}
                    src={item.url}
                    id={item.id}
                    alt={data?.name}
                  />
                </div>
              )
            })}
          </div>
        </div>
      )}
    </div>
  )
};

export default ProductPage;
