import axios from "axios";
import { Product } from "../../../types/product";

export const getProduct = (id: string): Promise<Product> =>
  axios.get(`/products/${id}`).then(res => res.data.data);
