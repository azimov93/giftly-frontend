import { Link } from "react-router-dom";
import styles from "./MainPage.module.scss";

const MainPage = () => {
  return (
    <div className={styles.wrapper}>
      <h1 className={styles.header}>
        Giftly Application
      </h1>
      <Link
        to="/products"
        className={styles.link}
      >
        Open List
      </Link>
    </div>
  )
}

export default MainPage;
