import { ChangeEvent, useCallback, useRef, useState } from "react";
import { queryCache, useMutation, useQuery } from "react-query";
import debounce from "lodash/debounce";
import Loader from "../../components/Loader";
import SearchInput from "../../components/SearchInput";
import SelectInput from "../../components/SelectInput";
import ProductCard from "../../components/ProductCard";
import PromotionItem from "../../components/Promotion";
import * as api from './modules/api';
import styles from "./ListPage.module.scss";

const ListPage = () => {
  const [searchText, setSearchText] = useState("");
  const [selectedVendor, setSelectedVendor] = useState("");

  const { isLoading: isProductsLoading, data: products = [] } = useQuery(
    "products",
    api.getProducts,
    {
      retry: false,
      refetchOnWindowFocus: false,
    }
  );

  const [invokeSearch] = useMutation(api.getSearch, {
    onSuccess: (data) => {
      queryCache.setQueryData("products", data);
    }
  });

  const { current: debouncedSearch } = useRef(debounce(invokeSearch, 250));

  const { isLoading: isVendorLoading, data: vendors = [] } = useQuery(
    "vendors",
    api.getVendors,
    {
      retry: false,
      refetchOnWindowFocus: false,
    }
  );

  const { isLoading: isPromotionLoading, data: promotion = null } = useQuery(
    "promotion",
    api.getPromotion,
    {
      retry: false,
      refetchOnWindowFocus: false,
    }
  );

  const initSearch = useCallback((name: string | null, vendor: string) => debouncedSearch({
    name: name || undefined,
    vendor: vendor || undefined,
  }), [debouncedSearch])

  const onSearchByName = useCallback((e: ChangeEvent<HTMLInputElement>) => {
    setSearchText(e.target.value);
    initSearch(e.target.value, selectedVendor);
  }, [selectedVendor, initSearch]);

  const onVendorSelect = useCallback((e: ChangeEvent<HTMLSelectElement>) => {
    setSelectedVendor(e.target.value);
    initSearch(searchText, e.target.value);
  }, [searchText, initSearch]);

  const isLoading = isProductsLoading || isVendorLoading || isPromotionLoading;

  return (
    <div>
      <div className={styles.inputsBlock}>
        <SearchInput
          value={searchText}
          onChange={onSearchByName}
          placeholder="Search by name"
        />
        <SelectInput
          options={vendors}
          value={selectedVendor}
          onChange={onVendorSelect}
        />
      </div>
      {isLoading && <Loader />}
      {!isLoading && (
        <div className={styles.list}>
          {!!products.length && products.map(({ order, ...item }, index) => {
            const isSearch = selectedVendor || searchText;
            const currentOrder = isSearch ? index : order;

            return (
              <ProductCard
                key={item.id}
                {...item}
                order={currentOrder}
              />
            )
          })}
          {!!products.length && promotion && (
            <PromotionItem {...promotion} />
          )}
        </div>
      )}
    </div>
  )
};

export default ListPage;
