import axios from "axios";
import { Product } from "../../../types/product";
import { Promotion } from "../../../types/promotion";

export const getProducts = (): Promise<Product[]> =>
  axios.get("/products").then(res => res.data.data);

export const getVendors = (): Promise<string[]> =>
  axios.get("/vendors").then(res => res.data.data);

export const getSearch = (search: { [key: string]: string | undefined }): Promise<Product[]> =>
  axios.get("/products", { params: search }).then(res => res.data.data);

export const getPromotion = (): Promise<Promotion> =>
  axios.get("/promotion").then(res => res.data.data);
