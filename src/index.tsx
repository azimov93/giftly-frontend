import React from 'react';
import ReactDOM from 'react-dom';
import axios from "axios";
import { BrowserRouter } from "react-router-dom";
import './assets/colors.scss';
import './assets/main.scss';
import App from './components/App';
import reportWebVitals from './reportWebVitals';

const API_HOST_URL = "https://giftly-application.herokuapp.com/api"

axios.defaults.baseURL = API_HOST_URL;

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
